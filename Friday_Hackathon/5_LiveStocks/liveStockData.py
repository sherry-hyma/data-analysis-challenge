import pandas as pd
from flask import Flask
import pandas_datareader as pdr
from flask_restplus import Resource, Api, fields

import requests
import json
import datetime

app = Flask(__name__)
api = Api(app)

model = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    }
    )

@api.route('/stock/<ticker>')
@api.param('ticker', 'enter the Stock Ticker you want to know the current price for:')
class Stock(Resource):
    def get(self, ticker):
        todays_date = datetime.datetime.now()-datetime.timedelta(1)
        ret = []

        stockslst=ticker.split(', ')
        for tick in stockslst:
            try:
                df = pdr.get_data_yahoo(tick, todays_date)[['Close']]
            except Exception as e:
                print('ERROR in get_data_yahoo...')
            df['Date'] = df.index
            temp = {'ticker': tick,'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),'price': df.iloc[-1]['Close']}
            ret.append(temp)
        return ret


if __name__ == "__main__":
    app.run(debug=True)