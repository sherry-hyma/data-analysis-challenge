from flask import Flask
from flask import jsonify
import json
from flask import request as req
import pandas as pd
import mariadb

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello'

@app.route('/ticker/<string:ticker>')
def get_price(ticker):
    try:
        conn = mariadb.connect(
            user="guest",
            password="",
            host="173.230.149.210",
            port=3306,
            database="test"

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        return


    df = pd.read_sql(('select * from `ALL` WHERE Ticker = "' + ticker + '";'), con=conn)
    return json.loads(df.to_json( orient='index'))


@app.route('/ticker/<string:ticker>/<string:date>')
def get_price_on_date(ticker, date):
    try:
        conn = mariadb.connect(
            user="guest",
            password="",
            host="173.230.149.210",
            port=3306,
            database="test"

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        return


    df = pd.read_sql(('select * from `ALL` WHERE Ticker = "' + ticker + '" AND Date = "'+date+'";'), con=conn)
    return json.loads(df.to_json( orient='index'))

@app.route('/date/<string:date>')
def get_date(date):
    try:
        conn = mariadb.connect(
            user="guest",
            password="",
            host="173.230.149.210",
            port=3306,
            database="test"

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        return


    df = pd.read_sql(('select * from `ALL` WHERE  Date = "'+date+'";'), con=conn)
    return json.loads(df.to_json( orient='index'))

if __name__ == '__main__':
    app.run()
