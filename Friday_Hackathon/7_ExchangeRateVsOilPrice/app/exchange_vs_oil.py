import logging
import datetime

from flask import Flask
from flask_restplus import Resource, Api
import joblib
import pandas as pd
import pandas_datareader as pdr

app = Flask(__name__)
api = Api(app)

model = joblib.load('exchange_vs_oil.joblib')

LOG = logging.getLogger(__name__)

def print_decimals(num):
    return float("%.2f" % round(num, 6))

@api.route('/price/<float:exchange_rate>')
@api.param('exchange_rate', 'CAD/USD exchange rate')
class OilPrice(Resource):

    def get(self, exchange_rate):
        
        start_date = datetime.datetime.now() - datetime.timedelta(1)

        try:
            cur_oil_price = pdr.get_data_yahoo("CL=F", start_date).iloc[0]['Adj Close']
        except Exception as ex:
            LOG.error('Error getting crude oil price data: ' + str(ex))

        try:
            cur_exchange_rate = pdr.get_data_yahoo("CADUSD=X", start_date).iloc[0]['Adj Close']
        except Exception as ex:
            LOG.error('Error getting CAD/USD exchange rate data: ' + str(ex))


        oil_price = model.predict([[exchange_rate]])

        return {'Current exchange rate (CAD/USD)': print_decimals(cur_exchange_rate),
                'Current crude oil price (USD/Barrel)': print_decimals(cur_oil_price),
                'Input exchange rate (CAD/USD): ': exchange_rate,
                'Expected oil price (USD/Barrel) for input': print_decimals(oil_price[0])}

if __name__ == '__main__':
    app.run(debug=True)
